// RobotBuilder Version: 2.0
//
// This file was generated by RobotBuilder. It contains sections of
// code that are automatically generated and assigned by robotbuilder.
// These sections will be updated in the future when you export to
// Java from RobotBuilder. Do not put any code or make any change in
// the blocks indicating autogenerated code or it will be lost on an
// update. Deleting the comments indicating the section will prevent
// it from being updated in the future.


package org.usfirst.frc3777.BeanCityBots2018.subsystems;

import org.usfirst.frc3777.BeanCityBots2018.RobotMap;
import org.usfirst.frc3777.BeanCityBots2018.commands.*;
import edu.wpi.first.wpilibj.command.Subsystem;
// BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=IMPORTS
import edu.wpi.first.wpilibj.AnalogAccelerometer;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Servo;

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=IMPORTS

//djh this smart helper doesn't work, but does work for the methods().???
/**
 * (djh) This subsystem will hold miscellaneous devices.
 */
public class subsChassis extends Subsystem {

    // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DECLARATIONS
    private final AnalogAccelerometer analogAccelerometer = RobotMap.subsChassisAnalogAccelerometer;
    private final AnalogGyro analogGyro = RobotMap.subsChassisAnalogGyro;
    private final Servo servo1 = RobotMap.subsChassisServo1;
    private final DigitalInput switchLeft = RobotMap.subsChassisSwitchLeft;
    private final DigitalInput switchRight = RobotMap.subsChassisSwitchRight;
    private final DoubleSolenoid dblsolSpare = RobotMap.subsChassisdblsolSpare;

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DECLARATIONS

    @Override
    public void initDefaultCommand() {
        // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DEFAULT_COMMAND


    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DEFAULT_COMMAND

        // Set the default command for a subsystem here.
        // setDefaultCommand(new MySpecialCommand());
    }

    @Override
    public void periodic() {
        // Put code here to be run every loop

    }

//-------------------------------------------------------------------------------
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    public static boolean m_switchleft = true;
    public static boolean m_switchRight = true;
    
    /**
     * (djh) This method() returns the boolean value of the RoboRIO's DIO raw logic input value,
     * of the "left" switch input.  The "left" switch is part of the subsChassis subsystem.
     * @param none There are no input parameters. (i.e. This method is not configurable.)
     * @return
     * If a DIO is left unconnected, it will be high and return: true.
     * <p>If a DIO is switched to ground, it will be low and return: false.
     */
    public boolean zgetSwitchLeft(){
    	m_switchleft = switchLeft.get();
    	return m_switchleft;
    }
    
    /**
     * (djh) This method() returns the boolean value of the RoboRIO's DIO raw logic input value,
     * of the "right" switch input.    The "left" switch is part of the subsChassis subsystem.
     * @return
     * If a DIO is left unconnected, it will be high and return: true.
     * <p>If a DIO is switched to ground, it will be low and return: false.
     */
    public boolean zgetSwitchRight(){
    	m_switchRight = switchRight.get();
    	return m_switchRight;
    }

}

