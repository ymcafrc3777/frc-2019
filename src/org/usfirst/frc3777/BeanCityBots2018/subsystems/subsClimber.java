// RobotBuilder Version: 2.0
//
// This file was generated by RobotBuilder. It contains sections of
// code that are automatically generated and assigned by robotbuilder.
// These sections will be updated in the future when you export to
// Java from RobotBuilder. Do not put any code or make any change in
// the blocks indicating autogenerated code or it will be lost on an
// update. Deleting the comments indicating the section will prevent
// it from being updated in the future.


package org.usfirst.frc3777.BeanCityBots2018.subsystems;

import org.usfirst.frc3777.BeanCityBots2018.RobotMap;
import org.usfirst.frc3777.BeanCityBots2018.commands.*;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;

// BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=IMPORTS
import edu.wpi.first.wpilibj.DoubleSolenoid;

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=IMPORTS


/**
 * (djh) This subsystem class is the 'Arm' of the robot.  The solenoid is controlled
 * by two double-solenoids.  One doublesolenoid controls the bottom port of the solenoid,
 * and the second doublesolenoid controls the top port of the solenoid.
 * @param none This is not configurable.
 * @return none
 */
public class subsClimber extends Subsystem {

    // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=CONSTANTS

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=CONSTANTS

    // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DECLARATIONS
    private final DoubleSolenoid dblsolArmBottom = RobotMap.subsClimberdblsolArmBottom;
    private final DoubleSolenoid dblsolArmTop = RobotMap.subsClimberdblsolArmTop;

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DECLARATIONS

    @Override
    public void initDefaultCommand() {
        // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DEFAULT_COMMAND

        setDefaultCommand(new cmdArmStop());

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=DEFAULT_COMMAND

        // Set the default command for a subsystem here.
        // setDefaultCommand(new MySpecialCommand());
    }

    @Override
    public void periodic() {
        // Put code here to be run every loop

    }

//-------------------------------------------------------------------------------
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    // The 'Arm' cylinder is controlled by two solenoids;
    //	one solenoid valve is connected to the bottom air port,
    //  and the other solenoid valve is connected to the top air port.
    
    //Use these Constants (aka final variables) to make code more readable.
    //  (Also, these can be easily changed in code, if changes are 
    //	made to how the cylinder is connected to the valves.)
    //		For ease of reading the 'method' code, only use these for the ArmTop.
    private static final DoubleSolenoid.Value FILL_TOP = Value.kReverse;
    private static final DoubleSolenoid.Value EXHAUST_TOP = Value.kForward;
    //		For ease of reading the 'method' code, only use these for the ArmBottom.
    private static final DoubleSolenoid.Value FILL_BOTTOM = Value.kForward;
    private static final DoubleSolenoid.Value EXHAUST_BOTTOM = Value.kReverse;

//djh There is no need for this method to exhaust both ends of cylinder.
//    public void zrelax(){
//    	// This will release pressure on both sides of the cylinder.
//    	dblsolArmTop.set(EXHAUST_TOP);				//vent the top
//    	dblsolArmBottom.set(EXHAUST_BOTTOM);		//vent the bottom
//    }
    /**
     * (djh) This changes states of the two doublesolenoids, to extend the 'Arm' cylinder.
     * @param none There is nothing to configure.
     * @return nothing
     */
    public void zreachhard(){
    	dblsolArmTop.set(EXHAUST_TOP);    			//both: vent the top, and
    	dblsolArmBottom.set(FILL_BOTTOM);			//pressurize bottom.
    }

//djh There is no need for this method().
//    // Don't use this function, YET.  Right now it's the same as 'zholdposition'.
//    public void zreach(){
//    	// This pressurizes both ends of the cylinder, thus might slowly reach out.
//    	dblsolArmTop.set(FILL_TOP);					//pressurize the top
//     	//TODO: maybe modulate the exhaust port, to gradually let out air????
//    	dblsolArmBottom.set(FILL_BOTTOM);			//pressurize the bottom
//    }
    
    /**
     * (djh) This changes states of the two doublesolenoids to put equal air pressure on both
     * ends of the 'Arm' cylinder.  The net result is that the cylinder may/will gradually
     * extend (due to less surface area on the top side of the cylinder's internal actuator
     * versus the bottom side of the cylinder's internal actuator).
     * @param none There is nothing to configure.
     * @return nothing
     */
	public void zholdposition(){
		// AT THIS TIME, THIS IS THE SAME AS "ZREACH()".
		dblsolArmTop.set(FILL_TOP);					//pressurize the top
		dblsolArmBottom.set(FILL_BOTTOM);			//pressurize the bottom
	}

//djh There is no need for this method().
//    public void zpullup(){
//     	// Maybe modulate the Bottom exhaust, to slowly let out air?????
//     	dblsolArmTop.set(FILL_TOP);		//pressurize the top
//     	//TODO: maybe modulate the exhaust port, to gradually let out air????
//     	dblsolArmBottom.set(EXHAUST_BOTTOM);		//vent bottom, maybe modulate this????
//    }

    /**
     * (djh) This changes states of the two doublesolenoids, to retract the 'Arm' cylinder.
     * @param none There is nothing to configure.
     * @return nothing
     */
    public void zpulluphard(){
        //This will be used at the end-of-game when robot is hooked, to pull itself up fast.
        //  For maximum speed, both top and bottom should be pressurized before calling this?????
    	dblsolArmTop.set(FILL_TOP);					//both: pressurize the top, and
    	dblsolArmBottom.set(EXHAUST_BOTTOM);		//vent the bottom air out.
    }

}

